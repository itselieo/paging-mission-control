import sys
from datetime import datetime
import json

def find_severity(data):
    levels = ['RED HIGH', 'YELLOW HIGH', 'YELLOW LOW', 'RED LOW']

    red_high_limit = float(data[2])
    yellow_high_limit = float(data[3])
    yellow_low_limit = float(data[4])
    red_low_limit = float(data[5])
    raw_val = float(data[6])

    diff1 = abs(raw_val - red_high_limit)
    diff2 = abs(raw_val - yellow_high_limit)
    diff3 = abs(raw_val - yellow_low_limit)
    diff4 = abs(raw_val - red_low_limit)

    diffs = [diff1, diff2, diff3, diff4]

    return levels[diffs.index(min(diffs))]


def parse_file(file_name):
    output_data = []

    with open(file_name) as input_file:
        for line in input_file:
            data = line.strip().split('|')
            output = {
                'satelliteId': int(data[1]),
                'severity': find_severity(data),
                'component': data[7],
                'timestamp': datetime.strptime(data[0], '%Y%m%d %H:%M:%S.%f').isoformat()
            }
            output_data.append(output)

    return output_data


if __name__ == '__main__':

    if len(sys.argv) < 2:
        print('Please enter the input file name: ')
        exit(1)

    file_name = sys.argv[1]

    output = parse_file(file_name)

    print(json.dumps(output, indent=4))
